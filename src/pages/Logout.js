import React from 'react';
import {useContext, useEffect} from 'react';
import {Navigate} from 'react-router-dom';
import UserContext from '../UserContext'
import Swal from 'sweetalert2'

export default function Logout(){
    
    const {setUser, unsetUser} = useContext(UserContext)

    unsetUser();

    useEffect(() => {
        setUser({
            id:null,
            isAdmin: null
        })

        const Toast = Swal.mixin({
            toast: true,
            iconColor: 'black',
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000,
            showCloseButton: true,
            customClass: {
              popup: 'add-cart'
          }
      })
        
        Toast.fire({
            icon: 'success',
            title: 'Logged Out'
        })
    },[])

    return(
        <Navigate to="/" />
        )
}