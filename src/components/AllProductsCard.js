import { Fragment, useState } from 'react'
import { Form, Modal, Button, Image} from 'react-bootstrap';
import { Link } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function AllProductsCard({allProdProp}){

    const { name, price, _id, description, isActive } = allProdProp
    const [id, setId] = useState('')
    const [productName, setProductName] = useState('')
    const [productDescription, setProductDescription] = useState('')
    const [productPrice, setProductPrice] = useState('')
    const [productStatus, setProductStatus] = useState(isActive)
    const [showEdit, setShowEdit] = useState('')

    // Opening Edit Modal
    const openEdit = (productId) => {
        setId(_id)
        setProductName(name)
        setProductDescription(description)
        setProductPrice(price)
        setProductStatus(isActive)
        setShowEdit(true)
    }

    // Closing Edit Modal
    const closeEdit = () => {
        setProductName('')
        setProductDescription('')
        setProductPrice(0)
        setShowEdit(false)
    }

    // Edit Product
    const editProduct = (e, productId) => {
        e.preventDefault()

        Swal.fire({
            title: 'Do you want to save the changes?',
            showDenyButton: true,
            showCancelButton: true,
            confirmButtonText: 'Yes',
            denyButtonText: 'No',
            customClass: {
              actions: 'my-actions',
              cancelButton: 'order-3',
              confirmButton: 'order-1 left-end',
              denyButton: 'order-2',
          }
      }).then((result) => {
        if (result.isConfirmed) {
            fetch(`https://hidden-stream-68962.herokuapp.com/products/${_id}`, {
                method: "PUT",
                headers: {
                    'Content-type': 'application/json',
                    Authorization: `Bearer ${localStorage.getItem('token')}`
                },
                body: JSON.stringify({
                    name: productName,
                    description: productDescription,
                    price: productPrice
                })
            })
            .then(res => res.json())
            .then(data => {
                if(data){
                    
                  Swal.fire('Saved!', '', 'success')
                  setProductName('')
                  setProductDescription('')
                  setProductPrice('')
                  closeEdit()
              } else {
                alert('Something went wrong.')
                closeEdit()
            }
        })
        } else if (result.isDenied) {
          Swal.fire('Changes are not saved', '', 'info')
      } else {
        closeEdit()
    }
})


  }

    // Setting the Product as Out of Stock
    const archive = (e) => {
        fetch(`https://hidden-stream-68962.herokuapp.com/products/${_id}/archive`, {
          headers: {
            Authorization: `Bearer ${localStorage.getItem('token')}`
        }
    })
        .then(data => {
          setProductStatus(false)
      })
    }

    // Setting the Product as In Stock
    const unarchive = (e) => {
      fetch(`https://hidden-stream-68962.herokuapp.com/products/${_id}/unarchive`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
      }
  })
      .then(data => {
        setProductStatus(true)
    })
  }

  // Deleting Product
  const deleteProduct = (e) => {
    Swal.fire({
      title: 'Are you sure you want to delete?',
      showDenyButton: true,
      showCancelButton: true,
      showConfirmButton: false,
      denyButtonText: `Delete`,
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire('Changes are not saved', '', 'info')
      } else if (result.isDenied) {
        fetch(`https://hidden-stream-68962.herokuapp.com/products/${_id}/deleteProduct`, {
          method: "DELETE",
          headers: {
            Authorization: `Bearer ${localStorage.getItem('token')}`
          }
        })
        Swal.fire('Product Deleted!', '', 'success')  

      }
    })
  }

  const src=`/images/products/${_id}.png`


  return (
    <Fragment>
    <tr>
    <td><Link to ={`/product/${_id}`}><Image src={src} className="all-image"/></Link></td>
    <td className="align-middle"><Link to ={`/product/${_id}`} className="all-product-name">{name}</Link></td>
    <td className="align-middle">{description}</td>
    <td className="align-middle">₱{price}.00</td>
    <>
    <td className="align-middle text-center all-products-action">
    <Button variant="outline-dark" onClick={() => openEdit(_id)}>Edit</Button>
    <Button variant="outline-dark" onClick={() => deleteProduct()} className="deleteBtn mt-2">Delete</Button>
    </td>
    </>
    </tr>

    <Modal show={showEdit} onHide={closeEdit} centered>
    <Form onSubmit={e => editProduct(e, id)}>
    <Modal.Header closeButton>
    <Modal.Title>Edit Product</Modal.Title>
    </Modal.Header>
    <Modal.Body>
    <Form.Group controlId="productName">
    <Form.Label>Name:</Form.Label>
    <Form.Control type="text" placeholder="Enter product name" value={productName} onChange={e => setProductName(e.target.value)} required/>
    </Form.Group>

    <Form.Group controlId="productDescription">
    <Form.Label>Description:</Form.Label>
    <Form.Control type="text" placeholder="Enter product description" value={productDescription} onChange={e => setProductDescription(e.target.value)} required/>
    </Form.Group>

    <Form.Group controlId="productPrice">
    <Form.Label>Price:</Form.Label>
    <Form.Control type="number" placeholder="Enter product price" value={productPrice} onChange={e => setProductPrice(e.target.value)} required/>
    </Form.Group>
    
    <Form.Group controlId="productAvailability" className="mt-3">
    <Form.Label>Available:</Form.Label>
    {
      productStatus ? 
      <Button variant="outline-dark" className="actions-btn" onClick={ e => archive(e) }>In Stock</Button>
      :
      <Button variant="outline-dark" className="actions-btn" onClick={ e => unarchive(e) }>Out of Stock</Button>
  }
  
  </Form.Group>

  </Modal.Body>
  <Modal.Footer>
  <Button variant="outline-dark" onClick={closeEdit}>Close</Button>
  <Button variant="outline-dark" type="submit">Submit</Button>
  </Modal.Footer>
  </Form>	
  </Modal>

  </Fragment>
  )

}