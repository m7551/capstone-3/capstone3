import { Fragment, useContext, useState } from 'react'
import { Button, Navbar, Nav, NavDropdown, Container, Modal, Form, Offcanvas} from 'react-bootstrap'
import { Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext'

export default function AppNavBar(){

    const {user} = useContext(UserContext);

    const [name, setName] = useState('')
    const [description, setDescription] = useState('')
    const [price, setPrice] = useState('')
    const [image, setImage] = useState('')
    const [category, setCategory] = useState('')
    const [showAdd, setShowAdd] = useState(false)
    const [show, setShow] = useState(false)
    const [navShow, setNavShow] = useState(false)
    const date = new Date().getFullYear()

    const handleClose = () => setShow(false)
    const handleShow = () => setShow(true)

    const showDropdown = (e) => {
        setNavShow(!navShow)
    }

    const hideDropdown = (e) => {
        setNavShow(false)
    }

    const openAdd = () => setShowAdd(true)
    const closeAdd = () => {
        setName('')
        setDescription('')
        setPrice('')
        setCategory('')
        setShowAdd(false)
        
    }

    // Adding product Modal
    const addProduct = (e) => {
        e.preventDefault()

        Swal.fire({
            title: 'Do you want to save the product?',
            showDenyButton: true,
            showCancelButton: true,
            confirmButtonText: 'Yes',
            denyButtonText: 'No',
            customClass: {
              actions: 'my-actions',
              cancelButton: 'order-3',
              confirmButton: 'order-1',
              denyButton: 'order-2',
          }
      }).then((result) => {
        if (result.isConfirmed) {
            fetch(`https://hidden-stream-68962.herokuapp.com/products`, {
                method: "POST",
                headers: {
                    'Content-type': 'application/json',
                    Authorization: `Bearer ${localStorage.getItem('token')}`
                },
                body: JSON.stringify({
                    name: name,
                    description: description,
                    price: price,
                    category: category
                })
            })
            .then(res => res.json())
            .then(data => {
                if(data){
                    Swal.fire('Product Successfully Added!', '', 'success')
                    closeAdd()
                } else {
                    Swal.fire('Error', 'Something went wrong', 'error')
                    closeAdd()
                }
            })
        } else if (result.isDenied) {
          Swal.fire('Changes are not saved', '', 'info')
      } else {
        closeAdd()
    }
})


  }

    // For greeting in Header
    const greet = () => {
        if(user.id !== null){
            if(user.isAdmin) {
                return `Hello Admin`
            } else {
                return `Hello ${user.name}`
            }
        }
    }    

    const offCanvasAddProduct = () => {
        handleClose()
        openAdd()
    }

    return(
        <Fragment>
        <div className="text-center header">We only accept orders from 9:30am - 8:30pm. Orders made beyond 8:30pm will be delivered the next day.</div>
        <Container>
        
        <Navbar expand="lg" className='mt-0 d-lg-block'>
        <Container>
        <Navbar.Brand as= {Link} to ="/" className="d-none d-md-block">
        <img
        src="/images/Cafe-logo.png" alt ="Logo"
        />
        </Navbar.Brand>
        <Navbar.Brand as= {Link} to ="/" className="d-md-none">
        <img
        src="/images/Cafe-logo-sm.png" alt ="Logo"
        />
        </Navbar.Brand>

        <Navbar.Toggle aria-controls="basic-navbar-nav" onClick={() => handleShow()} />

        <Navbar.Collapse id="basic-navbar-nav" className="d-none">
        <Nav className="ms-auto navbar-container">
        <NavDropdown 
            show = {navShow}
            onMouseEnter = {showDropdown}
            onMouseLeave = {hideDropdown}
            title="Menu" 
            id="basic-nav-dropdown">
        <NavDropdown.Item as = {Link} to ="/menu">All Products</NavDropdown.Item>
        <NavDropdown.Divider/>
        <NavDropdown.Item as = {Link} to ="/drinks">Drinks</NavDropdown.Item>
        <NavDropdown.Item as = {Link} to="/food">Food</NavDropdown.Item>
        <NavDropdown.Item as = {Link} to ="/side">Side Dish</NavDropdown.Item>
        </NavDropdown>
        {
            user.id !== null ?
            (user.isAdmin) ?
            <NavDropdown title={greet()} id="basic-nav-dropdown">
            <NavDropdown.Item className="dropdown-item" as={Button} onClick={openAdd}>Add Product</NavDropdown.Item>
            <NavDropdown.Item as = {Link} to ="/allProducts">All Products</NavDropdown.Item>
            <NavDropdown.Item className="dropdown-item" as = {Link} to="/orders/all">All Orders</NavDropdown.Item>
            <NavDropdown.Divider />
            <NavDropdown.Item as = {Link} to ="/logout">Logout</NavDropdown.Item>
            </NavDropdown>
            :
            <NavDropdown title={greet()} id="basic-nav-dropdown">
            <NavDropdown.Item className="dropdown-item" as= { Link } to ="/myCart">
            Cart
            </NavDropdown.Item>
            <NavDropdown.Item className="dropdown-item" as = {Link} to ="/myOrders">Orders</NavDropdown.Item>
            <NavDropdown.Divider />
            <NavDropdown.Item as = {Link} to ="/logout">Logout</NavDropdown.Item>
            </NavDropdown>

            :
            <Fragment>
            <Nav.Link className="nav-item" as= { Link } to ="/myCart">Cart</Nav.Link>
            <Nav.Link as = {Link} to = "/login" className="nav-item">Login</Nav.Link>
            <Nav.Link className="nav-item" as = {Link} to ="/register">Register</Nav.Link>
            </Fragment>
        }
        
        </Nav>
        </Navbar.Collapse>

        <Offcanvas show={show} onHide={handleClose} id="basic-navbar-nav" placement='end'>
        <Offcanvas.Header closeButton>
        <Offcanvas.Title>{greet()}</Offcanvas.Title>
        </Offcanvas.Header>
        <Offcanvas.Body>
        <Nav className="ms-auto navbar-container">
        <NavDropdown title="Menu" id="basic-nav-dropdown">
        <NavDropdown.Item as = {Link} to ="/menu" onClick={handleClose}>All Products</NavDropdown.Item>
        <NavDropdown.Divider/>
        <NavDropdown.Item as = {Link} to ="/drinks" onClick={handleClose}>Drinks</NavDropdown.Item>
        <NavDropdown.Item as = {Link} to="/food" onClick={handleClose}>Food</NavDropdown.Item>
        <NavDropdown.Item as = {Link} to ="/side" onClick={handleClose}>Side Dish</NavDropdown.Item>
        </NavDropdown>
        {
            user.id !== null ?
            (user.isAdmin) ?
            <>
            <Nav.Link className="nav-item dropdown-item" as={Button} onClick={offCanvasAddProduct}>Add Product</Nav.Link>
            <Nav.Link className="nav-item" as = {Link} to ="/allProducts" onClick={handleClose}>All Products</Nav.Link>
            <Nav.Link className="nav-item" as = {Link} to="/orders/all" onClick={handleClose}>All Orders</Nav.Link>
            <NavDropdown.Divider />
            <Nav.Link as = {Link} to ="/logout" className="nav-item" onClick={handleClose}>Logout</Nav.Link>
            </>
            
            :
            <>
            <Nav.Link className="nav-item" as= { Link } to ="/myCart" onClick={handleClose}>
            Cart
            </Nav.Link>
            <Nav.Link className="nav-item mb-5" as = {Link} to ="/myOrders" onClick={handleClose}>Orders</Nav.Link>
            <NavDropdown.Divider />
            <Nav.Link as = {Link} to ="/logout" className="nav-item" onClick={handleClose}>Logout</Nav.Link>
            </>
            

            :
            <Fragment>
            <Nav.Link className="nav-item" as= { Link } to ="/myCart" onClick={handleClose}>Cart</Nav.Link>
            <Nav.Link as = {Link} to = "/login" className="nav-item" onClick={handleClose}>Login</Nav.Link>
            <Nav.Link className="nav-item" as = {Link} to ="/register" onClick={handleClose}>Register</Nav.Link>
            </Fragment>
        }
        
        </Nav>
        
        
        </Offcanvas.Body>
        <div className="offcanvas-footer text-center">
        Cafe Leblanc | {date}
        </div>
        </Offcanvas>
        
        </Container>
        </Navbar>
        </Container>

        <Modal show={showAdd} onHide={closeAdd} centered>
        <Form onSubmit={e => addProduct(e)}>
        <Modal.Header closeButton>
        <Modal.Title>
        Add New Product
        </Modal.Title>
        </Modal.Header>
        <Modal.Body>
        <Form.Group className="mb-3 mt-2">
        <Form.Control 
        placeholder="Product Name"
        type="text"
        value={name}
        onChange={e => setName(e.target.value)}
        required
        />
        </Form.Group>

        <Form.Group className="mb-3 mt-4">
        <Form.Control 
        placeholder="Description"
        type="text"
        value={description}
        onChange={e => setDescription(e.target.value)}
        required
        />
        </Form.Group>

        <Form.Group className="mb-3 mt-4">
        <Form.Control 
        placeholder="Price"
        type="text"
        value={price}
        onChange={e => setPrice(Number(e.target.value))}
        required
        />
        </Form.Group>

        <Form.Group className="mt-4">
        <Form.Control 
        placeholder="Category"
        type="text"
        value={category}
        onChange={e => setCategory(e.target.value)}
        required
        />
        </Form.Group>

        {/* <Form.Group className="mt-4">
        <Form.Control 
        placeholder="Image"
        type="file"
        value={image}
        onChange={e => setImage(e.target.value)}
        required
        />
        </Form.Group> */}
        </Modal.Body>
        <Modal.Footer>
        <Button type="submit" variant="outline-dark">Submit</Button>
        <Button onClick={closeAdd} variant="outline-dark">Close</Button>
        </Modal.Footer>
        </Form>
        </Modal>
        </Fragment>
        )
}