import { useEffect, useState } from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { Routes, Route } from 'react-router-dom';
import AppNavBar from './components/AppNavBar';
import Footer from './components/Footer';
import AllOrders from './pages/AllOrders';
import AllProducts from './pages/AllProducts';
import Drinks from './pages/DrinksOnly';
import Food from './pages/FoodOnly';
import Error from './pages/Error'
import Home from './pages/Home';
import Login from './pages/Login'
import Logout from './pages/Logout'
import MyCart from './pages/MyCart';
import Orders from './pages/Orders';
import Products from './pages/Products';
import Register from './pages/Register';
import Side from './pages/SideOnly';
import Specific from './pages/Specific';
import {UserProvider} from './UserContext'
import './App.css';

function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })

  const unsetUser = () => {
    localStorage.clear()
  }

  useEffect(() => {
    fetch(`https://hidden-stream-68962.herokuapp.com/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => {
      if(typeof data._id !== 'undefined'){
        setUser({
          id: data._id,
          name: data.firstName,
          isAdmin: data.isAdmin
        })
      } else {
        setUser({
          id: null,
          name: null,
          isAdmin: null
        })
      }
    })
  }, [])

  return (
    <UserProvider value = {{user, setUser, unsetUser}}>
    <Router>
    <AppNavBar />
    <Routes>
    <Route exact path="/" element = {<Home />} />
    <Route exact path="/register" title="Register" element = {<Register />} />
    <Route exact path="/login" element = {<Login />} />
    <Route exact path="/logout" element = {<Logout />} />
    <Route exact path="/allProducts" element={<AllProducts />} />
    <Route exact path="/orders/all" element={<AllOrders />} />
    <Route exact path="/product/:productId" element={<Specific/>}/>
    <Route exact path="/myCart" element={<MyCart/>}/>
    <Route exact path="/myOrders" element={<Orders/>}/>
    <Route exact path="/menu" element ={<Products />} />
    <Route exact path="/drinks" element ={<Drinks />} />
    <Route exact path="/food" element ={<Food />} />
    <Route exact path="/side" element ={<Side />} />
    <Route path="*" element={<Error />} />
    </Routes>
    </Router>
    <Footer />
    </UserProvider>
    );
  }

  export default App;
